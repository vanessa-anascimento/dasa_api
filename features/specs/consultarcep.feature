# language: pt

    Funcionalidade: Consultar CEP

    Esquema do Cenario: Consultar CEP com Sucesso

    Dado que o usuário queira consultar um endereço através de um CEP
    Quando inserir o <cep> e pesquisar
    Então deverá retornar as informações do endereço

    Exemplos:
    |cep       |
    |"04520013"|
    |"06223020"|
    |"01001000"|
    
   


   