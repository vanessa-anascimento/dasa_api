# language: pt

    Funcionalidade: Consultar CEP Inválido

    Esquema do Cenario: Consultar CEP Inválido

    Dado que o usuário queira consultar um endereço através de um CEP
    Quando inserir o <cep> inválido e pesquisar
    Então deverá retornar um erro 

    Exemplos:
    |cep          |
    |"546546546"  |
    |"5464564565" |
    |"54645645645"|
    