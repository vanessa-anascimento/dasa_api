#!/usr/bin/env ruby

Dado("que o usuário queira consultar um endereço através de um CEP") do

  puts "\n\nInicio dos Testes\n\n"

end

Quando("inserir o {string} e pesquisar") do |cep|

    @cep                  = cep.to_s
   
    endpoint             = $api['consultacep']
    mount                = endpoint.gsub("<cep>", @cep)

    @get =  HTTParty.get(mount,:headers => {"Content-Type" => 'application/json'})

    puts ("\n\nEndpoint enviado: #{mount}\n\n")
end

Então("deverá retornar as informações do endereço") do

	       puts ("\nResponse: \n" + @get.body)
           parse = JSON.parse(@get.body, object_class: OpenStruct)
           @idibge = parse.ibge 

           puts ("\nIBGE: \n" + @idibge)
end

